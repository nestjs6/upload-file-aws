import { NotFoundException } from '@nestjs/common';

export const fileFilter = (
  req: Express.Request,
  file: Express.Multer.File,
  callback: (error: Error, acceptFile: boolean) => void,
) => {
  if (!file) return callback(new Error('Archivo vacio'), false);

  const fileExtension = file.mimetype.split('/')[1];
  const fileExtensionName = file.originalname.split('.').pop();
  const validExtensions = [
    'pdf',
    'doc',
    'docx',
    'png',
    'jpg',
    'jpeg',
    'xsl',
    'xlsx',
  ];

  if (
    validExtensions.includes(fileExtension) &&
    validExtensions.includes(fileExtensionName)
  ) {
    return callback(null, true);
  }
  callback(new NotFoundException('Formato de archivo no permitido'), false);
};
