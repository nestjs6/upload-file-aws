import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import { v4 as uuid } from 'uuid';
import { CreateUploadDto } from './dto/create-upload.dto';

@Injectable()
export class UploadService {
  private readonly s3Client = new S3Client({
    region: this.configService.getOrThrow('AWS_S3_REGION'),
  });

  constructor(private readonly configService: ConfigService) {}

  async create(createUploadDto: CreateUploadDto) {
    const fileExtension = createUploadDto?.mimetype.split('/')[1];
    const fileName = `${uuid()}.${fileExtension}`;
    await this.s3Client.send(
      new PutObjectCommand({
        Bucket: 'aws-nest-upload-files',
        Key: fileName,
        Body: createUploadDto.buffer,
      }),
    );

    return { file: fileName };
  }
}
